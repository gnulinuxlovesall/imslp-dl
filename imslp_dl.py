import argparse
import requests
import re
import random
import os
from time import sleep
import subprocess as sp

parser = argparse.ArgumentParser(description='Download sheet music without a waittime. Download free music for FLO')
parser.add_argument('path', type=str, help='path to serial')
args = parser.parse_args()
#print(args.path)

# This would be the point to differentiate url, number, or web text
# For scraping ctrl-a copy paste, re.findall('#\d*', args.path)
#if s.isdecimal():

class ImslpDL:

    def __init__(self, url):
        self.imprisioned_url_with_a_dash_of_help_us_by_giving_us_money_so_we_can_continue_making_public_domain_music_unfree = url

    def find_freedom(self):
        
        found_freedom = False
#        while not found_freedom:
#            if url.is_disclaimer:
#                url = Disclaimer(

#    def disclaimer_to_waitpage(self)
#    def waitpage_to_freedom(self)
#    disclaimer_url

#class ImslpPage
#    def __new__(cls, url):
        #?
    #search = """<h1 id="firstHeading" class="firstHeading pagetitle page-header">Disclaimer</h1>"""
#.midi

#.sheetmusic
#.fullscores
#.parts
#.arr
#.scores
#.arr


def dl(imprisioned_url_with_a_dash_of_help_us_by_giving_us_money_so_we_can_continue_making_public_domain_music_unfree):
    global imslp_index
    WGET_WAIT=0.3
    USER_AGENT = "\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36\""
    LIMIT_RATE = "50k" # This could be randomixed
    headers = {'user-agent': USER_AGENT}
    search = """<h1 id="firstHeading" class="firstHeading pagetitle page-header">"""
    url = imprisioned_url_with_a_dash_of_help_us_by_giving_us_money_so_we_can_continue_making_public_domain_music_unfree
    almost_freed_url = False
    r = requests.get(url, headers=headers)
    s = r.text
    if s.find("firstHeading") >= 0:
        ### Debug
        print("Redirected to disclaimer url")
        f = open("disclaimer.html",'w')
        f.write(s)
        f.close()
        ###
        c = s.find("firstHeading")
        is_disclaimer = True
        c += len(search) + 1
        c += s[c:].find('href') + 4 + 2
        wait_url = "https://imslp.org" + s[c: c + s[c:].find('"') ]
        print("Wait url: {}".format(wait_url))
        url = wait_url

    sleep(WGET_WAIT * (1+random.random()))

    if ".pdf" in url or ".mid" in url:
        almost_freed_url = url
    else:
        r = requests.get(url, headers=headers)
        s = r.text
        ### Debug
        f = open("waitpage.html",'w')
        f.write(s)
        f.close()
        ###
        if s.find("sm_dl_wait") >= 0:
            c = s.find("sm_dl_wait")
            is_waitpage = True
            if False:
                sleep(15 + random.random())
            c += s[c:].find('data-id="') + len('data-i="') + 1

            # This is called "almost freed" because it is still on an unfree server
            almost_freed_url = s[c: c + s[c:].find('"') ]
            almost_freed_url = almost_freed_url.replace('&#58;', ':')
            print("Almost freed url: {}".format(almost_freed_url))

            #r = requests.get(almost_freed_url, headers=headers)
            #f = open(almost_freed_url[almost_freed_url.find('IMSLP'):],'w')
            #f.write(r.text)
            #f.close()
        elif s.find("sm_dl_new") >= 0:
            c = s.find("sm_dl_new")
            c += s[c:].find('>') + 1
            print("Failed to download. IMSLP says \"{}\"".format(s[c: c + s[c:].find('<')]) )

        if almost_freed_url:
            f = open("downloads/almost_freed_urls",'w')
            f.write("{}\t{}\t{}".format(imslp_index, "y", almost_freed_url))
            f.close()

            fname = almost_freed_url[almost_freed_url.find('IMSLP'):]
            # Privacy Popen
            # p = sp.Popen("wget --wait={} --random-wait --limit-rate={} -e robots=off -U {} -O {} {}".format(WGET_WAIT, LIMIT_RATE, USER_AGENT, fname, almost_freed_url))
            # Simple Popen
            # I hate Popen, Again
            # p = sp.Popen("wget -O {} {}".format(fname, almost_freed_url))
            # I don't know why this doesn't work. Something with the shell garbage. It is not intuitive and I consistantly guess and check until I get it right.
            os.system("wget -O {} {}".format(fname, almost_freed_url))

        sleep(WGET_WAIT * (1+random.random()))


imslp_index = args.path
url = args.path
imprisioned_imslp_index_with_a_dash_of_help_us_by_giving_us_money_so_we_can_continue_making_public_domain_music_unfree = url
dl(imprisioned_imslp_index_with_a_dash_of_help_us_by_giving_us_money_so_we_can_continue_making_public_domain_music_unfree)
