# imslp-dl and flomslp.org

# Why have I copied Tartini's IMSLP archive and I'm serving it for free at [flomslp.org](flomslp.org)?

I've been using [IMSLP](https://imslp.org/wiki/Main_Page) since before it's subscription model. IMSLP was at one point an actually free service. The subscription model was started just before 2016. I have for years seen countless 15 second wait times. This is a part of IMSLP I'm very experienced with. Since this day, I have been expecting and waiting for a better solution that improves the situation of accessing free music for free. I have yet to hear of another service. I have for many years planned on taking this on myself. Now in late 2021, I am making my plans public for an actually free library of free music scores.

In December 2020, I started [imslp-dl](https://gitlab.com/gnulinuxlovesall/imslp-dl/-/tree/master), a script to download from IMSLP without a wait wall. I have a strong intent of copying the entire archive over a period of months. 

# IMSLP is gratis, not free

IMSLP claims that it is a free service.

This is from [FAQ](https://imslp.org/wiki/IMSLP:FAQ)
> Is IMSLP free?
> 
> Yes. All visible files on the site can be downloaded for free by anyone visiting the site. Since the advent of subscriptions at the very end of 2015, certain popular public domain files (now about one-third of the total number) have been subject to a 15-second delay before the link to download appears. Subscribers who are logged in have no download delays, see no ads and can listen to the library of licensed commercial recordings by simply clicking on one of the album covers which can be seen on numerous work pages.


Non-members don't pay with money, but there is still a price. 

One price is 15 seconds of wasted time per score unless they have installed ublock origin which speeds this up to about a second. For all who live with this, this 15 seconds does nothing to further IMSLP's funding.

Another price is that users don't have control over their private data unless they have installed privacy badger. Like most commercial websites, IMSLP invites google, as well as trackers, to disregard our human tech right to privacy.

The last price is the eyesore of the ads users have to see unless they have installed ublock origin.

# Why don't you want to pay the subscription price?

If I chose to pay the subscription price, it does not make the situation better for anyone except Project Petrucci LLC. which owns imslp. I will not feel better because I am paying for something that should be free as in freedom. This money also doesn't do anything for anyone else. This money does not go towards making IMSLP the way it used to be. 

A better way for any user to deal with the wait wall is to use ublock origin or imslp-dl, and instead pay $2.99 a month to the EFF.

This is not about $2.99 a month, which of all possible monthly charges has to be one of the lowest. This is about where that money is going.

Instead of $2.99 a month for something that I feel ethically opposed too, I am spending $5 a month on a DigitalOcean droplet in Toronto to serve Tartini's archive as of Nov 3 2021. There is no extra cost if bandwidth is under 1,000 GB. The entire IMSLP archive is about 500 GB. This means for less than the price of most hardcopy scores, I could give the entire archive away to two people in a month. But to account for the seldom times U.S. and Canadian public domain copyright laws don't align, I would need an actual harddrive in Canada to serve from. Am I breaking the law if I send any of these questionable scores from the U.S. to my Canadian server, to the be sent to whoever is requesting the score? I don't know for sure. What I do know is that the copyright system is completely broken and backwards. 

[IMSLP:Site_support](https://imslp.org/wiki/IMSLP:Site_support)

A $5 droplet with 20 GB of diskspace can comfortably hold the 2.8 GB Tartini archive. For $5 a month, I could give the archive to 357 people at a price of 1.4 cents each.


This problem of sending content for no cost has already been solved. The Pirate Bay is a gratis service. To cut down on bandwidth, IMSLP could link composer's archives to torrents.

Archive.org could also serve zips of composer's archives. Archive.org is completely free and does not have upload or download limits. 

I understand that this takes work. But IMSLP is a user driven service. It already depends on the work of it's users. The service itself also asks users to mirror the site. This problem of cutting server costs is absolutely completely solvable. IMSLP just is going about everything the wrong way. 

Just what is IMSLP's philosophy. This is what they say:

[IMSLP:Goals](https://imslp.org/wiki/IMSLP:Goals)

Philosophy
We at the IMSLP believe that music should be something that is easily accessible for everyone. To this end, we have created the IMSLP in order to provide music scores free of charge to anyone who has internet access. IMSLP will always be freely accessible.

Goals
The ultimate goal of the IMSLP is to gather all public domain music scores, in addition to the music scores of all contemporary composers (or their estates) who wish to release them to the public free of charge. However, another main goal of IMSLP is to facilitate the exchange of musical ideas outside of compositions: for example, the analysis of a particular piece of music. Therefore, feel free to create/edit a page with your analysis of a particular piece (please use the "Discussion" link on the work page of that particular piece). For general discussions, and IMSLP-related questions, score requests, etc. you can use the forums. We hope to build a growing community of dedicated musicians and music lovers, who can use this site as a platform for enjoying music.


What is more important to IMSLP, is it that scores are free or that IMSLP is the only way to get public domain scores? There are multiple solution to this problem. If Feldmahler really wants music accessible, he would either himself upload the archive to archive.org, or he would make it clear that all who have any part of the archive are allowed to do so. If uploading to https://archive.org or https://thepiratebay.org/ is too difficult, I am happy to do so myself once he sends me the archive.

If IMSLP instead intends to protect it's archive, it has proven that the profits of Project Petrucci LLC are actually a more important goal than free access to music.

That's where imslp-dl comes in. We need a way to move copy the content of IMSLP and serve it in a free manner as soon as possible. I am beyond impatient at this point. I can wait another year max. A downloader running at reasonable rates and distributed among computers it well within IMSLP's server capacities. 

# What do others think?

I am definitely not alone in my disappointment of IMSLP. I have put some quotes below from the Dec 27, 2015 forum post where it was anounced.

# Forum responses
[forum post](http://imslpforums.org/viewtopic.php?f=1&p=40225)

I am deeply troubled by these changes.

IMSLP has had an incredible reputation, up to now, of being open-access platform hosting open-access files that is motivated by the joy of music, not money. These changes punish every user who does not donate (which will still be a large majority), and damage the reputation of the website. Indeed, waiting for a file is reminiscent of the virus-ridden, advertisement-heavy illegal download websites of the past.

We should also remember, despite contributors making significant efforts over the years, IMSLP's work has no protection. There is absolutely nothing stopping someone from making a mirror of the files on the site, either for their own benefit, or to take IMSLP's former place as the website of public domain music.

I am surprised, and disappointed, by the changes, given the focus of IMSLP until now. One might claim that keeping access open to all files is not problematic, but, I promise you, it is problematic when access is made so frustratingly difficult. In this respect, the website now feels little different to those websites that copy IMSLP's work and attempts to charge users for it.

I urge you to reconsider these changes. There is an obvious and generous audience prepared to make donations, but not if IMSLP changes from an open provider of public domain music to a subscription based provider. Users should be encouraged to donate by being reminded of the work of IMSLP contributors, not by being forced into doing so.

I am also troubled by the recent changes made to IMSLP.

As only a user of the Iibrary, I am not aware of IMSLP's financials. If it was made clear to users, upfront like Wikipedia, what IMSLP's financial straits were, I am sure that others like me would gladly donate.

Is imslp.org a charity? Are there any tax form 990s filed under imslp.org's name?

The current approach is even worse than being vague: it is punishing every user who does not pay.

What is the reason that IMSLP is not a charity?

I agree with the concerns expressed so far in this thread. I'm not as articulate as many others, and I don't know much about how the financial side of running IMSLP works, but I would love to know more, even if it's in the simple format of "$X needed to cover costs for this month - $Y already raised."

But overall I agree with those who describe the changes as punishing those who do not contribute, and I think it is a sad day and a huge step backwards for IMSLP and its reputation.

The Wikimedia Foundation provides financial reports and plans
